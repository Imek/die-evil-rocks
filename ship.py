"""Core code for space ship entities."""

# Library imports
import math

# Engine imports
import engine.state
import engine.control
import engine.entity
import engine.physics
from engine.vector import Vector2, UP

class SpaceShipState(engine.state.WorldObjState):
	"""The observable state of a ship in the game world."""

	def __init__(self, *args, **kwargs):
		super(SpaceShipState, self).__init__(*args, **kwargs)
		# TODO: Orientation (and angular motion) data should be in the rigid
		# body code (when we implement that to replace using the particle)
		self.angular_thrust = 0.0 # Currently equivalent to acceleration
		self.angular_velocity = 0.0 # radians/second
		self.set_orientation( kwargs.get('initial_orientation', math.pi/2) )

	# TODO: Move orientation-specific code out of here once we have a rigid
	# body class.

	def get_orientation(self):
		"""Return the orientation as an angle about the object's origin in radians."""
		return self.orientation

	def set_orientation(self, angle):
		"""Set our orientation as a rotation about the object's origin."""
		assert angle >= 0.0
		self.orientation = angle
		self.facing = None # Set on demand (see get_facing)

	def rotate(self, angle):
		"""Rotate this object by a given angle about the object's origin."""
		new_orientation = (self.orientation + angle) % (math.pi * 2)
		self.set_orientation(new_orientation)

	def get_facing(self):
		"""Get (possibly calculating) the facing vector for this object."""
		if self.facing is None:
			self.facing = UP.make_copy()
			self.facing.rotate(self.orientation) 
		return self.facing


class SpaceShipController(engine.control.EntityController):
	"""A controller class for space ships."""

	def __init__(self, ship):
		assert isinstance(ship, SpaceShipEntity)
		super(SpaceShipController, self).__init__(ship)

	def update(self, time):
		"""Our update function to perform ship-specific work."""
		engine.control.EntityController.update(self, time)

		# Simple rotation based on the above angular thrust value.
		# TODO: When we have ships as rigid bodies, this should be done
		# properly there in the physics code.
		state = self.entity.state
		entity_type = self.entity.entity_type
		ang_vel = state.angular_velocity
		ang_vel += state.angular_thrust * time
		ang_vel = min(max(ang_vel, entity_type.min_angular_velocity),
					  entity_type.max_angular_velocity)
		if ang_vel != 0.0:
			if entity_type.angular_damping is not None:
				ang_vel *= pow(entity_type.angular_damping, time)

			state.rotate(ang_vel * time)
		state.angular_velocity = ang_vel


class SpaceShipEntity(engine.entity.Movable):
	"""Generic space ship entity."""
	state_class = SpaceShipState
	physics_class = engine.physics.Particle
	controller_class = SpaceShipController

	def update(self, time):
		"""Run through the ship's update logic for a game update step."""

		# Update control
		self.controller.update(time)

		# Update physics (& position)
		self.physics.update(time)

		# Reset accumulated turn thrust
		self.state.angular_thrust = 0.0