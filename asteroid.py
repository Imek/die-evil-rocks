# Library imports
import math
import random

# Engine imports
import engine.state
import engine.entity
from engine.vector import Vector2


class AsteroidState(engine.state.WorldObjState):
	"""Represents a small individual asteroid object."""

	def __init__(self, min_radius=22, max_radius=32, num_edges=20, **kwargs):
		"""Initialise a randomised asteroid from the given parameters."""
		
		super(AsteroidState, self).__init__(**kwargs)

		# TODO: use values from config object
		assert 0 < min_radius <= max_radius
		assert num_edges > 2

		max_radius += 1 # for randrange
		theta = 2/float(num_edges) * math.pi
		prev_edge = Vector2( 0, -random.randrange(min_radius, max_radius) )
		points = [prev_edge]
		for i in range(1, num_edges):
			radius = random.randrange(min_radius, max_radius)
			prev_edge = prev_edge.rotated_copy(theta)
			prev_edge.set_length(radius) # TODO: More efficient to make 1's then multiply?
			points.append(prev_edge)

		self.points = points

	def get_points(self):
		"""The points of an asteroid entity are on the generated state."""
		return self.points


class AsteroidEntity(engine.entity.Movable):

	state_class = AsteroidState


class BasicAsteroid(engine.entity_config.EntityType):

	# ENTITY CLASS FOR CONSTRUCTION
	entity_class = AsteroidEntity

	visual = engine.entity_config.PolygonVisualConfig(
		points=None, # Determined by state
		colour=(255, 255, 255) )
