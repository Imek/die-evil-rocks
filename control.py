# Engine imports
import engine.vector as vector
from engine.control import ControlScheme, ContinuousAction, Thrust, Turn


# Set up actions

# Ship top-down
forward_thrust = ContinuousAction(
	"forward", Thrust, vector.UP, relative=True)
reverse_thrust = ContinuousAction(
	"reverse", Thrust, vector.DOWN, relative=True)
turn_left = ContinuousAction(
	"turn_left", Turn, direction=False)
turn_right = ContinuousAction(
	"turn_right", Turn, direction=True)

# Ship side-scrolling
up_thrust = ContinuousAction(
	"up", Thrust, vector.UP, relative=False)
down_thrust = ContinuousAction(
	"down", Thrust, vector.DOWN, relative=False)
left_thrust = ContinuousAction(
	"left", Thrust, vector.LEFT, relative=False)
right_thrust = ContinuousAction(
	"right", Thrust, vector.RIGHT, relative=False)

# Set up control schemes
top_down = ControlScheme('ShipTopDown')
top_down.bind_key('w', forward_thrust)
top_down.bind_key('s', reverse_thrust)
top_down.bind_key('a', turn_left)
top_down.bind_key('d', turn_right)

side_scrolling = ControlScheme('ShipSideScrolling')
side_scrolling.bind_key('w', up_thrust)
side_scrolling.bind_key('s', down_thrust)
side_scrolling.bind_key('a', left_thrust)
side_scrolling.bind_key('d', right_thrust)
