Introduction
============

Die Evil Rocks is a work-in-progress PyGame learning project of mine.
Not much to look at here right now, except for some playing around
with experimental game code architecture.

Author & Licence
================

Name: Joe Forster
E-mail: me@joeforster.com
Website: http://joeforster.com
Licence: See accompanying file LICENCE.txt
