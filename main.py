"""Main entry-point for Die Evil Rocks.

Currently contains set-up and game loop code, as well as some loose stuff
that needs tidying up."""

# Library imports
import pygame

# Engine imports
import engine
import engine.zone
import engine.game_config
from engine.vector import Vector2

# Game-specific imports
from ship_config import BasicSpaceShip
from asteroid import BasicAsteroid


# This initialises the engine (including the display e.g. pygame screen)
config = engine.game_config.GameConfig
engine.init(config)

width, height = config.get_resolution()

# TODO: Move game loop into engine so the last of this pygame-specific stuff
# can go!
fps_clock = pygame.time.Clock()
fps = config.fps

# Game set-up
# TODO: Refactor this into a GameCore class with a pygame-specific implementation.
# TODO: Set up initial game state (scene entities etc.) from a config
demo_zone = engine.zone.TestZone.build_entity('DemoZone')

initial_player_pos = Vector2(width/2, height/2)
player_ship = BasicSpaceShip.build_entity(
	'Player', zone=demo_zone, position=initial_player_pos)
demo_zone.add_object(player_ship)

test_asteroid_pos = Vector2(width/4, height/4)
test_asteroid = BasicAsteroid.build_entity(
	'Asteroid_1', zone=demo_zone, position=test_asteroid_pos)
demo_zone.add_object(test_asteroid)


while True:
	# Determine time since last game loop (just constant FPS right now)
	assert fps > 0, "WTF are you doing with this FPS value: %r" % fps
	time = 1.0 / fps

	# Update all game logic for the active zone and its contents
	demo_zone.update(time)

	# Re-draw the active zone and its contents
	# TODO: More efficient drawing of just the changed regions!
	demo_zone.draw()
	pygame.display.flip()

	# Next frame.
	fps_clock.tick(fps)
