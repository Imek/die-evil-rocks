import engine_config

def init(game_config):
	"""Perform engine-specific initialisation."""
	engine_config.EngineConfig.init_library_interfaces(game_config)