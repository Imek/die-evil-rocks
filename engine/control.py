import input_manager

class EntityController(object):
	"""Base entity controller class, which manages and applies ControlActions."""

	def __init__(self, entity):
		# Map action ID to affectors that remain until they are removed
		self.affectors = {}
		self.counts = {}
		self.entity = entity

	def has_affector(self, act_name):
		"""Return whether we already have an affector with the given action name."""
		return act_name in self.affectors

	def count_affectors(self, act_type):
		"""Return the number of affectors of the given type on this controller."""
		return self.counts.get(act_type, 0)

	def add_affector(self, affector):
		"""Create and apply an affector for this action to the given controller."""
		if not isinstance(affector, Affector):
			raise TypeError( "Extected Affector, got %s" % type(affector) )
		act_name = affector.get_name()
		#print "add_affector %s" % act_name
		if self.has_affector(act_name):
			return False
		else:
			self.affectors[act_name] = affector
			self.counts[act_name] = self.counts.get(act_name, 0) + 1
			affector.on_add(self.entity)
			return True	

	def remove_affector(self, act_name):
		"""Given an ContinuousAction name, remove its affector from this controller."""
		#print "remove_affector %s" % act_name
		if act_name not in self.affectors:
			return False
		else:
			removed_act = self.affectors[act_name]
			del self.affectors[act_name]
			self.counts[act_name] -= 1
			removed_act.on_remove(self.entity)
			return True

	def update(self, time):
		"""Basic update function to apply all sustained actions for this update cycle."""
		for affectors in self.affectors.itervalues():
			affectors.update(time, self.entity)


class Action(object):
	"""A thing an entity can do, that can be applied to its controller to affect a change in state."""
	
	def __init__(self, name):
		self.name = name

	def get_name(self):
		"""Get the unique (per-entity) identifier of this binding."""
		return self.name

	def apply(self, controller):
		"""Apply this action to the given controller."""
		return True

	def remove(self, controller):
		"""Remove the effects of this action from the given controller, if applicable."""
		return True


class ContinuousAction(Action):
	"""Represents an action that has a continuous effect on its entity while applied."""

	def __init__(self, name, affector_cls, *args, **kwargs):
		Action.__init__(self, name)
		if not issubclass(affector_cls, Affector):
			raise TypeError(
				"Expected sub-class of Affector, got %s" %\
					affector_cls.__class__.__name__)
		self.affector_cls = affector_cls
		self.act_args = args
		self.act_kwargs = kwargs

	def apply(self, controller):
		"""Create and apply an affector for this action to the given controller."""
		act_name = self.name
		if controller.has_affector(act_name):
			return False
		else:
			affector = self.affector_cls(
				act_name, controller.entity.entity_type,
				*self.act_args, **self.act_kwargs)
			return controller.add_affector(affector)

	def remove(self, controller):
		"""Remove this action's affector from the given controller."""
		return controller.remove_affector(self.name)


class InputBinding():
	"""A binding of some input stimulus to an Action."""
	def __init__(self, action):
		if not isinstance(action, Action):
			raise TypeError( "Expected Action, got %s" % type(action) )
		self.action = action


class KeyBinding(InputBinding):
	"""An InputBinding for the keyboard."""
	def __init__(self, key, action):
		InputBinding.__init__(self, action)
		self.key = key


class ControlScheme():
	"""Represents a control scheme in the game with a set of input bindings."""

	def __init__(self, name):
		"""Create and register a new control scheme."""
		self.name = name
		self.input_bindings = {}

	def get_bindings(self, input_method):
		return self.input_bindings.get(input_method, {})

	def bind_key(self, key, action):
		"""Create a KeyBinding on this control scheme."""
		bindings = self.input_bindings.setdefault(input_manager.IM_KEYBOARD, {})
		bindings.setdefault(key, []).append( KeyBinding(key, action) )


class Affector():
	"""Represents an affector applied by a ContinuousAction on an EntityController."""

	def __init__(self, name, entity_type):
		self.name = name
		self.entity_type = entity_type

	def get_name(self):
		return self.name

	@classmethod
	def get_action_type(cls):
		return cls.__name__

	def on_add(self, entity):
		"""Optional code to call once when this action is added to an entity."""
		pass

	def on_remove(self, entity):
		"""Optional code to call once when this action is removed from an entity."""
		pass

	def update(self, time, entity):
		"""Update the given Entity instance with this sustained action."""
		pass


class Thrust(Affector):
	"""Affector that applies some thrust in some direction to an entity."""
	
	def __init__(self, name, entity_type, direction, relative):
		"""Make a thrust action in the given direction for the given entity.

		relative, if True, makes thrust relative to the entity's facing
		direction - otherwise, thrust direction is absolute (world)."""
		assert direction.is_unit(), "Expected unit vector for Thrust"
		self.thrust = direction * entity_type.get_max_thrust(name)
		self.relative = relative
		Affector.__init__(self, name, entity_type)

	def update(self, time, entity):
		"""Alter the thrust attribute of a (thrustable) object's state."""
		# OPTIMISE: When relative, only need to perform rotation if the facing rotates
		if self.relative:
			thrust_force = self.thrust.rotated_copy(
				entity.state.get_orientation() )
		else:
			thrust_force = self.thrust
		entity.physics.add_force(thrust_force)


# TODO: This will need changing once turning is on the Physics object
# rather than the (ship) entity itself.
class Turn(Affector):
	"""Action to toggle the angular thrust increasing/decreasing rate of a movable."""
	def __init__(self, name, entity_type, direction):
		if direction:
			self.angular_thrust = entity_type.max_angular_thrust
		else:
			self.angular_thrust = 0-entity_type.max_angular_thrust
		Affector.__init__(self, name, entity_type)

	def update(self, time, entity):
		"""TODO: apply angular force to the entity"""
		entity.state.angular_thrust += self.angular_thrust * time
