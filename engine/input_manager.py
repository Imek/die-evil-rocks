class InputManager():
	"""Game input manager, which translates controller input into actions."""

	def __init__(self, entity):
		"""Set up the input_state and possible entity to manage."""
		self.entity = entity
		self.input_state = set()

	def set_input(self, *keys_mods):
		"""Given zero or more tuples of the form (event_type, key_code, modifier_code),
		update our input state for the next update."""
		self.input_state = set(keys_mods)

	def update(self, time):
		"""Given the current input state and the amount of time that has
		passed since the last update, produce and distribute the action
		events to the appropriate entities."""
		pass

# Input methods used for configuration
IM_KEYBOARD = "Keyboard"