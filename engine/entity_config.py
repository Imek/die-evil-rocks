"""Code for representing game configuration."""

import os

# TODO: Config loadable from files + validation checking
# (will want for environment generation and control configs and such)

class EntityType(object):
	
	# Control scheme set if this is a player-controller entity
	control_scheme = None

	@classmethod
	def build_entity(cls, *args, **kwargs):
		"""Construction function to make an entity of this type.

		You can override this to add any code to construct any constutuent
		objects or do any other set-up needed on construction, e.g. adding
		objects to a zone."""
		# Set if the entity should have an InputManager determined by its Zone.
		return cls.entity_class(*args, entity_type=cls, **kwargs)


class PolygonVisualConfig():
	"""Specification of an object that is drawn as a polygon."""

	drawer_type = 'Polygon'

	def __init__(self, points, colour):
		self.points = points
		self.colour = colour


class SpriteVisualConfig():
	"""Visual for a sprite."""

	drawer_type = 'Sprite'
	_img_dir = os.path.join('data', 'sprites')

	def __init__(self, image_path):
		"""Construct a sprite visual from the appropriate info."""
		self.image_path = os.path.join(self._img_dir, image_path)


class BGVisualConfig():
	"""Visual that is just a background image."""

	drawer_type = 'Background'
	_img_dir = os.path.join('data', 'bg')

	def __init__(self, image_path, repeat=False):
		"""Construct a BG visual from the appropriate info."""
		self.image_path = os.path.join(self._img_dir, image_path)
		self.repeat = repeat
