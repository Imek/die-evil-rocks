# TODO: configurable imports by name? (couldn't get it working...)
import pygame_interface

class EngineConfig():
	"""Configuration class for common engine code."""

	@classmethod
	def get_interface_module(cls, module):
		"""Import the appropriate interface module for the given component."""
		return getattr(pygame_interface, module)

	@classmethod
	def init_library_interfaces(cls, game_config):
		"""Initialise each library interface."""
		pygame_interface.init(game_config)
