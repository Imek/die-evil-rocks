"""Core state code for Die Evil Rocks game engine"""

from collections import namedtuple

import vector


class EntityState(object):
	"""Base class for any kind of state belonging to an entity."""


class WorldObjState(EntityState):
	"""Base state class for objects in the game world."""

	def __init__(self, entity_type, zone, position):
		"""Construct a world object at the given position."""
		self.entity_type = entity_type
		# TODO: zones
		assert position is None or isinstance(position, vector.Vector2)
		self.zone = zone
		self.position = position

	def get_zone(self):
		return self.zone

	def get_position(self):
		return self.position

	def get_orientation(self):
		return 0 # TODO: Refactor from ship stuff
