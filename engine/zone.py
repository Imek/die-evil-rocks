import scene
import game_config
import engine_config
import entity_config

input_manager = engine_config.EngineConfig.get_interface_module('input_manager')

class Zone(scene.WorldScene):
	"""A game zone, which manages its constituent entities and their control."""
	repeat = True

	def __init__(self, *args, **kwargs):
		super(Zone, self).__init__(*args, **kwargs)
		self.input_managers = {} # Map entity name to InputManager
		self.zoom_level = self.entity_type.initial_zoom

	def add_object(self, entity):
		"""Add an object to the Zone, creating an InputManager if needed."""
		entity = super(Zone, self).add_object(entity)
		if entity is None:
			return # Did not add it

		name = entity.name
		if name not in self.input_managers:
			if entity.entity_type.control_scheme is not None:
				try:
					input_method = game_config.GameConfig.input_method
					input_method = getattr(input_manager, input_method+'Input')
				except AttributeError:
					raise NotImplementedError(
						"Unavailable input method '%s' on entity '%s'" %\
							(input_method, name) )

				self.input_managers[name] = input_method(entity)

	def remove_object(self, object_or_name):
		"""Remove an object from the Zone, removing its InputManager if applicable."""
		entity = super(Zone, self).remove_object(object_or_name)
		if entity is None:
			return # Didn't remove anything

		name = entity.name
		if name in self.input_managers:
			del self.input_managers[name]

	def update(self, time):
		"""Update function to do input managers before base object update."""
		for input_manager in self.input_managers.itervalues():
 			input_manager.update(time)
		super(Zone, self).update(time)


class TestZone(entity_config.EntityType):
	"""Test space zone with crappy programmer art background."""
	entity_class = Zone
	visual = entity_config.BGVisualConfig("space.png", repeat=True)
	initial_zoom = 0.182
