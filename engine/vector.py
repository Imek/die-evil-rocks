import math

class Vector2():
	"""Simple 2-dimensional vector class."""

	# Tolerance value to use for approximate zero checks (pun not intended)
	# NOTE: This is done against squared length for efficiency
	_zero_tolerance = 0.0000001

	# Construction & copying

	def __init__(self):
		self.x = 0.0
		self.y = 0.0

	def __init__(self, x, y):
		self.x = x
		self.y = y

	def make_copy(self):
		"""Copy a vector's values into a new instance."""
		return Vector2(self.x, self.y)

	# String representation functions

	def __repr__(self):
		return "Vector2(x=%r, y=%r)" % (self.x, self.y)

	def __str__(self):
		return repr(self)

	# Length utilities

	@classmethod
	def make_zero(cls):
		"""Make and return a zero vector."""
		return cls(0.0, 0.0)

	def zero(self):
		"""Zero this vector."""
		self.x = self.y = 0.0

	def is_zero(self, tolerance=_zero_tolerance):
		"""Determine whether this is (near) a zero vector."""
		if tolerance <= 0.0:
			return self == ZERO
		else:
			return self.length_sq() < tolerance

	def length(self):
		"""Calculate and return the length of this vector."""
		return math.sqrt(self.x*self.x + self.y*self.y)

	def length_sq(self):
		"""Return the square of this vector's length."""
		return self.x*self.x + self.y*self.y

	# TODO: This isn't very reliable with rotations & floating-point 
	# innacuracy. Is there a clever way around that?
	def is_unit(self, tolerance=_zero_tolerance):
		"""Return whether this is a unit (1-length) vector."""
		return abs(1.0 - self.length_sq()) < tolerance

	def normalise(self):
		"""Normalise this vector."""
		length = self.length()
		self.x /= length
		self.y /= length

	def set_length(self, new_length):
		"""Set the length of this vector by normalising and multiplying."""
		self.normalise()
		self.mult( float(new_length) )

	def limit_length(self, min_len=None, max_len=None):
		"""Limit this vector's length to within a minimum and maximum"""
		
		if min_len is not None and max_len is not None:
			assert min_len <= max_len
		elif min_len is None and max_len is None:
			return # No limits baby

		len_sq = self.length_sq()
		if max_len is not None and len_sq > max_len*max_len:
			self.set_length(max_len)
		elif min_len is not None and len_sq < min_len*min_len:
			self.set_length(min_len)

	# Function for rotating points/vectors about the origin.
	@staticmethod
	def rotate_points(x, y, angle):
		if angle == 0.0:
			return x, y
		else:
			ca = math.cos(angle)
			sa = math.sin(angle)
			return (x*ca - y*sa), (x*sa + y*ca)

	def get_rotated_points(self, angle):
		return self.rotate_points(self.x, self.y, angle)

	def rotate(self, angle):
		"""Rotate this vector about the origin given an angle in radians.."""
		if angle != 0.0:
			self.x, self.y = self.rotate_points(self.x, self.y, angle)

	def rotated_copy(self, angle):
		v = self.make_copy()
		v.rotate(angle)
		return v

	#===================#
	# VECTOR ARITHMETIC #
	#===================#

	# Addition

	def add(self, x, y):
		"""Add the given values to this vector."""
		self.x += x
		self.y += y

	def add_vector(self, vec):
		"""Add the given vector to this vector."""
		assert isinstance(vec, Vector2)
		self.add(vec.x, vec.y)

	def __add__(self, other):
		"""Overload addition, creating a new Vector2 and using add."""
		assert isinstance(other, Vector2)
		vec = self.make_copy()
		vec.add_vector(other)
		return vec

	# Scalar multiplication

	def mult(self, val):
		"""Multiply this vector given a scalar value."""
		self.x *= val
		self.y *= val

	def __mul__(self, val):
		"""Overload the multiplication operator for scalar multiplication."""
		vec = self.make_copy()
		vec.mult(val)
		return vec


	# Equality

	def equals(self, other):
		"""Check equality with another vector-like object."""
		return isinstance(other, Vector2) and self.x == other.x and self.y == other.y

	def __eq__(self, other):
		"""Overload equality to call equals."""
		return self.equals(other)


# Constants

ZERO = Vector2.make_zero()
UP = Vector2(0, -1)
DOWN = Vector2(0, 1)
LEFT = Vector2(-1, 0)
RIGHT = Vector2(1, 0)
