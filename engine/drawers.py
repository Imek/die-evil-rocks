class EntityDrawer():
	"""Something that can draw an entity of a given type."""

	def __init__(self, entity):
		"""Initialise a drawer given our entity's state."""
		self.entity_type = entity.entity_type
		self.entity_state = entity.state

	def draw(self):
		raise NotImplementedError()