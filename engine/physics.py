"""Physics module for the 2D top-down game.

In general, we use SI units for physical properties such as mass and so on."""

from vector import Vector2

import state

class Force(Vector2):
	"""A simple force with a direction and a magnitude."""
	pass

class Particle:
	"""A simple particle object with a position in the world."""

	# Constructor and property setters

	def __init__(self,
				 entity_state,
				 mass=1.0,
				 velocity=None,
				 damping=None,
				 max_speed=None):
		"""Construct a stationary particle with an initial position vector.

		entity_state  -- A WorldObjState for this particle to control.
		mass          -- A mass value for this particle in kilogrammes
		velocity      -- An initial velocity for this particle (optional)
		damping       -- Damping to apply to this particle's motion (optional)
		max_speed     -- A maximum scalar speed to enforce (optional)
		"""
		assert isinstance(entity_state, state.WorldObjState)
		self.state = entity_state
		self.set_mass(mass)
		self.velocity = Vector2.make_zero() if velocity is None else velocity
		self._force_accumulator = Vector2.make_zero()
		self.set_max_speed(max_speed)
		self.set_damping(damping)

	def get_position(self):
		"""Give the position vector of this particle."""
		return self.state.position

	def set_mass(self, mass=1.0):
		"""Set the new mass of this particle, updating inv_mass too."""
		assert mass > 0, "Particle must have positive mass! Given: %r" % mass
		mass = self.mass = float(mass)
		self.inv_mass = 1.0 / mass

	def set_max_speed(self, max_speed=None):
		"""Set the maximum speed, or unset it if none given."""
		if max_speed is None:
			self.max_speed = self.max_speed_sq = None
		else:
			max_speed = float(max_speed)
			assert max_speed >= 0.0,\
				"Negative max_speed on particle! Given: %r" % max_speed
			self.max_speed = max_speed
			self.max_speed_sq = max_speed*max_speed

	def set_damping(self, damping=None):
		"""Set the constant damping value, if given, or unset it otherwise."""
		if damping is None:
			self.damping = None
		else:
			damping = float(damping)
			assert damping >= 0.0,\
				"Negative damping on particle! Given: %r" % damping
			self.damping = damping

	# Force accumulator functions

	def add_force(self, force):
		"""Attach a force vector to our force accumulator."""
		self._force_accumulator += force


	# Update code

	def update(self, time):
		"""Update our physical state, given a time period since the last one."""

		vel = self.velocity

		# Apply forces to determine acceleration, and clear the accumulator.
		# Might as well use the accumulator to store the calculated
		# acceleration, as we will clear it immediately.
		accel = self._force_accumulator
		accel.mult(self.inv_mass*time)

		# Apply acceleration (simple way) and clear accumulator.
		vel.add_vector(accel)

		#print "New velocity: %r" % vel.length()

		self._force_accumulator.zero()

		# If we have damping, apply it now.
		if self.damping is not None:
			vel.mult( pow(self.damping, time) )

		# If we have a maximum speed, check it now.
		if self.max_speed is not None and vel.length_sq() > self.max_speed_sq:
			vel.set_length(self.max_speed)

		# Finally update the position
		self.state.position.add_vector(vel*time)
