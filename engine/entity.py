import state
import physics
import drawers
import engine_config

drawers_interface = engine_config.EngineConfig.get_interface_module('drawers')


class Entity(object):
	"""Represents a controllable, drawable entity in the game environment."""

	def __init__(self, name, entity_type, position=None, zone=None, state_args={}, visual=None):
		"""Base Entity constructor, at least taking a name and type."""
		self.name = name
		self.entity_type = entity_type

		if self.state_class is not None:
			self.state = self.state_class(
				entity_type=entity_type, zone=zone, position=position, **state_args)
			assert isinstance(self.state, state.WorldObjState)
		else:
			self.state = None

		visual = entity_type.visual
		if visual is not None:
			drawer_type = visual.drawer_type
			drawer_class = getattr(drawers_interface, drawer_type+'Drawer', None)
			if drawer_class is None:
				raise NotImplementedError(
					"Drawer type '%s' not in interface!" % drawer_type)
			self.drawer = drawer_class(self)
			assert isinstance(self.drawer, drawers.EntityDrawer)
		else:
			self.drawer = None
		
	def draw(self):
		"""Draw this entity in the game world, if applicable."""
		if self.drawer is not None:
			self.drawer.draw()

	# Class variables for construction - should be set by implementor.
	state_class = None


class WorldObject(Entity):
	"""An entity that is an individual object in the game world.

	This type is used to distinguish an individual object from an entity
	container (such as a WorldScene)."""
	pass


class Movable(WorldObject):
	"""An object that can move itself via control and/or physics."""

	# Type-specific class variables.
	# TODO: Combine physics with controller (since they conceptually part of the same thing)?
	physics_class = None
	controller_class = None

	def __init__(self, name, entity_type, physics_args={}, controller_args={}, **kwargs):
		"""Helper function to build a movable entity of this class.

		Optionally, arguments can be given to be passed in to the
		physics and/or controller constructors as appropriate.
		"""
		super(Movable, self).__init__(name, entity_type, **kwargs)

		if self.physics_class is not None:
			physics_defaults = getattr(entity_type, 'physics_args', {})
			physics_args = dict(physics_defaults, **physics_args)
			entity_physics = self.physics_class(self.state, **physics_args)
		else:
			entity_physics = None

		self.physics = entity_physics

		if self.controller_class is not None:
			controller_defaults = getattr(entity_type, 'controller_args', {})
			controller_args = dict(controller_defaults, **controller_args)
			controller = self.controller_class(self, **controller_args)
		else:
			controller = None

		self.controller = controller

	def update(self, time):
		"""Update this entity's state given the seconds passed since last call."""
		pass # Implement!
