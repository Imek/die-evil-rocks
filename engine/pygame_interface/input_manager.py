import sys
import pygame

from ..input_manager import InputManager, IM_KEYBOARD

class KeyboardInput(InputManager):
	"""Basic input manager for a keyboard controlling just the player ship."""

	def __init__(self, entity):
		"""Pass the controlled entity through to the super-class."""
		InputManager.__init__(self, entity)
		self._update_mappings()
		
	def _update_mappings(self):
		"""Update our internal representation of the key mappings."""
		bindings = self.entity.entity_type.control_scheme.get_bindings(IM_KEYBOARD)
		self._key_mappings = {}
		for key, binding in bindings.iteritems():
			self._key_mappings[getattr(pygame, 'K_'+key)] = binding

	def update(self, time):
		"""Take our keyboard input and produce actions for the player."""
		controller = self.entity and self.entity.controller
		if controller is None:
			return

		# Grab pygame input events since last update
		input_state = set()
		for event in pygame.event.get():
			# Handle quit event
			# TODO: Move somewhere better?
			if event.type is pygame.QUIT:
				pygame.quit()
				sys.exit()
			elif event.type in (pygame.KEYDOWN, pygame.KEYUP):
				input_state.add((event.key, event.mod, event.type == pygame.KEYDOWN))

		# Convert inputs into control actions
		add_actions = remove_actions = []
		for key_code, modifier_code, down in input_state:
			bindings = self._key_mappings.get(key_code)
			if bindings:
				for binding in bindings:
					if down:
						binding.action.apply(controller)
					else:
						binding.action.remove(controller)
