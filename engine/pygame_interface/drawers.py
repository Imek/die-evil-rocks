"""PyGame-specific object code for drawing visual elements."""

import pygame

from ..vector import Vector2, ZERO
from ..drawers import EntityDrawer

# We currently just support a single static pygame screen.
# NOTE Initialised on interface init (__init__.py)
screen = None
game_config = None

black = (0, 0, 0)

# Map image name to loaded surface
_surfaces = {}

# TODO: May make more sense to refactor this system to actually
# create a Drawer instance for each entity, since most of them need
# to hold per-entity data such as surfaces. Then we won't need this
# messy globals() use - just put the drawer on the entity at creation
# and you can do entity.draw()

class PolygonDrawer(EntityDrawer):

	DEFAULT_COLOUR = (0, 0, 0)

	@staticmethod
	def _generate_poly_points(points, position=ZERO, orientation=None):
		for p in points:
			x, y = p.x, p.y
			if orientation is not None:
				x, y = Vector2.rotate_points(x, y, orientation)
			yield x+position.x, y+position.y

	def draw(self):
		"""Given an appropriate entity, draw it as a polygon."""
		pointTuples = self._generate_poly_points(
			self.entity_state.get_points(),
			position=self.entity_state.position,
			orientation=self.entity_state.get_orientation() )
		pygame.draw.polygon(screen, self.entity_type.visual.colour, tuple(pointTuples))


class BackgroundDrawer(EntityDrawer):

	def __init__(self, entity):
		"""Initialise the surface needed for drawing our entity."""
		image_path = entity.entity_type.visual.image_path
		self.surface = pygame.image.load(image_path).convert()
		EntityDrawer.__init__(self, entity)

	def draw(self):
		visual = self.entity_type.visual
		# Draw the surface, repeating if specified in the config.
		# TODO: Is this the most efficient way? (no need to fill every frame..)
		screen.fill(black)
		if visual.repeat:
			w, h = game_config.get_resolution()
			w_inc, h_inc = self.surface.get_width(), self.surface.get_height()
			assert w > 0
			assert h > 0
			assert w_inc > 0
			assert h_inc > 0
			x = 0
			while x < w:
				y = 0
				while y < h:
					screen.blit( self.surface, (x, y) )
					y += h_inc
				x += w_inc
		else:
			screen.blit( self.surface, (0, 0) )


# TODO: use dirty sprites for efficiency
class SpriteDrawer(EntityDrawer):

	def __init__(self, entity):
		"""Initialise the SpriteDrawer for drawing a sprite entity."""
		EntityDrawer.__init__(self, entity)

		image_path = entity.entity_type.visual.image_path
		zoom_level = self.entity_state.get_zone().zoom_level
		# TODO make sprite object
		surface = _surfaces.get(image_path)
		if surface is None:
			surface = _surfaces[image_path] = pygame.image.load(image_path).convert()

		# Determine image size, possibly resizing
		width, height = surface.get_width(), surface.get_height()
		if zoom_level != 1.0:
			width, height = int(width*zoom_level), int(height*zoom_level)
			surface = pygame.transform.smoothscale( surface, (width, height) )
		self.surface = surface

		# Centre coordinates
		# TODO: update stuff when zoom changes
		self.centre_x = width/2
		self.centre_y = height/2

		self.sprite = pygame.sprite.DirtySprite()
		self.sprite.image = self.surface
		self.sprite.rect = self.surface.get_rect()

	def _update_position(self):
		pos = self.entity_state.get_position()
		self.sprite.rect.topleft = (pos.x-self.centre_x, pos.y-self.centre_y)

	def draw(self):
		# TODO: Optimise drawing & position update
		self._update_position()
		screen.blit(self.sprite.image, self.sprite.rect)
