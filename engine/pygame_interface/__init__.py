import pygame

import drawers
import input_manager

game_config = None
screen = None

def init(config):
	"""Initialise the pygame system for use by our engine code."""
	global game_config, screen
	game_config = config
	if screen is None:
		pygame.init()
		screen = pygame.display.set_mode( game_config.get_resolution() )
		drawers.screen = screen
		drawers.game_config = game_config