import input_manager

class GameConfig():
	"""Core configuration class for the game."""

	# TODO: Pull from INI file

	screen_width = 1280
	screen_height = 720
	fps = 60

	input_method = input_manager.IM_KEYBOARD

	@classmethod
	def get_resolution(cls):
		return cls.screen_width, cls.screen_height
