from entity import Entity, WorldObject, Movable

class WorldScene(Entity):
	"""Base class for drawable scenes."""

	def __init__(self, *args, **kwargs):
		self.objects = {} # Maps name to Entity reference
		self.movables = {} # Movables added (redundant for quick access)
		return super(WorldScene, self).__init__(*args, **kwargs)

	def add_object(self, entity):
		"""Add the given object to this scene, returning it if it's now in there."""
		if not isinstance(entity, WorldObject):
			raise TypeError( "Expected WorldObject, got %s: %r" % (type(entity), entity) )

		existing = self.objects.get(entity.name)
		if existing is None:
			self.objects[entity.name] = entity
			if isinstance(entity, Movable):
				self.movables[entity.name] = entity
			return entity # Success
		else:
			assert existing is entity,\
				"Different objects (%s/%s) with same name '%s'!" %\
					(existing, entity, entity.name)
			return None # Already in there

	def remove_object(self, object_or_name):
		"""Given an objec or name, remove and return (if successful) an object."""
		if isinstance(object_or_name, WorldObject):
			name = object_or_name.name
		else:
			name = object_or_name
		try:
			removed_entity = self.objects[name]
			del self.objects[name]
			if isinstance(removed_entity, Movable):
				del self.movables[name]
			return removed_entity
		except KeyError:
			return None

	def update(self, time):
		"""Main update function for this scene and its containing entities."""
		for movable in self.movables.itervalues():
			movable.update(time)

	def draw(self):
		"""Draw this scene and the appropriate visible objects."""
		# Basic drawing code just draws everything indiscriminately.
		super(WorldScene, self).draw()
		for entity in self.objects.itervalues():
			entity.draw()
