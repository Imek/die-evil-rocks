# Library imports
import math

# Engine imports
import engine.entity_config
import engine.control
from engine.vector import Vector2, UP, DOWN, LEFT, RIGHT

# Game-specific imports
import ship
import control

# TODO: Lots of stuff needs moving into this config system.
class BasicSpaceShip(engine.entity_config.EntityType):
	"""Base specification for a space ship, with some defaults for test use."""

	# ENTITY CLASS FOR CONSTRUCTION
	entity_class = ship.SpaceShipEntity

	# MOVEMENT & CONTROL
	zone_input = True # Input manager determined by current zone
	max_angular_velocity = math.pi*1.9
	min_angular_velocity = -math.pi*1.9
	# TODO: proper counter-thrust braking instead of using damping 
	# to slow down?
	angular_damping=0.06

	max_angular_thrust = 600.0

	_thrust_default = 350.0
	_thrust_values = {
		'forward': 400.0,
		'reverse': 150.0,
	}

	@classmethod
	def get_max_thrust(cls, act_name):
		"""Return the maximum thrust value for the given thrust action."""
		return cls._thrust_values.get(act_name, cls._thrust_default)

	auto_brake_turn=True

	control_scheme = control.side_scrolling

	# VISUALS
	visual = engine.entity_config.SpriteVisualConfig("hermes_side.png")

	# PHYSICS ARGS
	# TODO: angular_damping moved in here
	physics_args = {
		'max_speed': 180.0,
		'damping': 0.50
	}
